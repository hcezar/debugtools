﻿using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;
using System.Text.RegularExpressions;
using System;

namespace RPSCDebugTools
{
    public class DebugTools
    {
        StringBuilder result;
        public DebugTools()
        {
            result = new StringBuilder();
            Debug.Listeners.Add(new TextWriterTraceListener(new StringWriter(result)));
            Debug.IndentLevel = 0;
            this.Wrap = false;
        }

        /// <param name="winSetup">String type in format \<width\>w\<height\>h/param>
        public void DebugShowResult(string winSetup)
        {
            int width = 600;
            int height = 500;

            Regex widthRE = new Regex(@"\d+W", RegexOptions.IgnoreCase);
            Regex heightRE = new Regex(@"\d+H", RegexOptions.IgnoreCase);

            Match widthmatch = widthRE.Match(winSetup);
            Match heigthmatch = heightRE.Match(winSetup);

            if (widthmatch.Success)
            {
                width = int.Parse(widthmatch.Value.ToLower().Replace("w", string.Empty));
            }

            if (heigthmatch.Success)
            {
                height = int.Parse(heigthmatch.Value.ToLower().Replace("h", string.Empty));
            }

            DebugShowResult(width, height);
        }
        public void DebugShowResult(int Width = 600, int Height = 500)
        {
            DebugDialog dialog = new DebugDialog(result, Width, Height);
            dialog.DialogWrap = this.Wrap;
            if (result.Length > 0) dialog.ShowResult();
        }

        public StringBuilder Result
        {
            get { return result; }
        }

        public bool Wrap { get; set; }
    }
}
