﻿using Autodesk.Revit.DB;
using Microsoft.Win32;
using System;
using System.Text;
using System.Windows;
using System.IO;
using System.Windows.Controls;

namespace RPSCDebugTools
{
    /// <summary>
    /// Interaction logic for Window.xaml
    /// </summary>
    public partial class DebugDialog : Window
    {
        public DebugDialog(StringBuilder result, int Width, int Height)
        {
            this.DialogWrap = false;

            InitializeComponent();
            DataContext = new { Text = result.ToString() };
            this.Width = Width;
            this.Height = Height;
        }

        public void ShowResult()
        {
            wrapCB.IsChecked = this.DialogWrap;
            this.ShowDialog();
        }

        public bool DialogWrap { get; set; }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            if (button.Name == "buttonCopy")
            {
                Clipboard.SetText(textboxResult.Text);
                MessageBox.Show("Content copied to Clipboard");
            }

            if (button.Name == "buttonSave") 
            {
                SaveFileDialog sfd = new SaveFileDialog
                {
                    Filter = "Text Files (*.txt)|*.txt"
                };

                if (sfd.ShowDialog() == true)
                {
                    StreamWriter sw = new StreamWriter(sfd.FileName);
                    sw.Write(textboxResult.Text);
                    sw.Close();
                    MessageBox.Show("Content Saved to " + sfd.FileName, "Debug Result");
                } 
            }
        }
    }
}
